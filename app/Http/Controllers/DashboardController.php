<?php

namespace App\Http\Controllers;
use App\Models\CarbOptions;
use App\Models\ClientMeal4;
use App\Models\ClientWeight;
use App\Models\CookBook;
use App\Models\EatOutOptions;
use App\Models\ClientDinner;
use App\Models\ClientLunch;
use App\Models\CoachClient;
use App\Models\FatOptions;
use App\Models\FruitOptions;
use App\Models\Picture;
use App\Models\ProteinOptions;
use App\Models\User;
use App\Models\VegetableOptions;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\ClientBreakfast;
use Intervention\Image\Facades\Image;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->hasRole('coach')) {

            $user = Auth::user();
           /* $clientnames = CoachClient::where('Coachid', $user->id)->get();*/

            /*$allclient = CoachClient::first('Clientid');*//*join one more table*/
                $allclients = DB::table('users')
                ->join('coachclient', 'users.id', '=', 'coachclient.Clientid')
                ->select('users.*')->where('coachclient.Coachid', $user->id)
                ->get();

            return view('coach-dashboard',compact( /*'clientnames',*/'allclients'));

        } elseif
        (Auth::user()->hasRole('client')) {

            $user=Auth::user();
            $weights = ClientWeight::where('Clientid',$user->id)/*->select('weight','created_at')*/->orderBy('added_date')->get();

            return view('client-dashboard',['weights'=>$weights]);

        }
        elseif (Auth::user()->hasRole('administrator')) {

            return view('dashboard');
        }
    }

    public function clientdiet()
    {
        $user = Auth::user();

        $image = ClientBreakfast::select('image')->where('Clientid', $user->id)->first();
        $lunch = ClientLunch::select('image')->where('Clientid', $user->id)->first();
        $dinner = ClientDinner::select('image')->where('Clientid', $user->id)->first();
        $meal4 = ClientMeal4::select('image')->where('Clientid', $user->id)->first();
        $usertdee = CoachClient::where('Clientid',$user->id)->get('TDEE');


        $tdee = $usertdee;
        $dataArray = json_decode($tdee, true);
        $tdee = $dataArray[0]['TDEE'];

       if ($user && $image &&  $lunch && $dinner && $meal4) {
            $image2 = $image->image ;
            $imagedata = Image::make($image2);

            $image3 = $lunch->image ;
            $imagedata2 = Image::make($image3);

           $image4 =  $dinner->image ;
           $imagedata3 = Image::make($image4);

           $image5 =  $meal4->image ;
           $imagedata4 = Image::make($image5);


           return view('client-diet',compact('tdee'))->with(['imagedata'=> $imagedata,
               'imagedata2'=>$imagedata2 , 'imagedata3'=>$imagedata3 , 'imagedata4'=>$imagedata4]
           );
        }
        else{
            $coachid = DB::table('CoachClient')->where('Clientid',$user->id)
            ->value('Coachid');
            $coachname = User::find($coachid)->name;
            $message = "Diet Plan not yet / completely created by Coach";
            return view('diet-not-created',['coachname' => $coachname],compact('tdee'))->with('message', $message);
        }

    }
    public function clientworkout()
    {
        return view('client-workout');
    }
    public function eatout()
    {
        $eatout = EatOutOptions::all();
        return view('eat-out-options' , compact('eatout'));
    }
    public function downloadcookbook()
    {
        $pdfFile = CookBook::find(1);
        $pathToFile = storage_path('app/public/' . $pdfFile->path);

        return response()->download($pathToFile);
    }
    public function coachcreate()
    {
       /* $coach = Auth::user();
        $all = User::all();

        $breakfast =DB::table('users')
            ->join('clientbreakfast', 'users.id', '=', 'clientbreakfast.Clientid')
            ->select('clientbreakfast.image')->where('clientbreakfast.Coachid',$coach->id)->where('clientbreakfast.Clientid',$all)
            ->exist();*/

        return view('coach-create');
    }
    public function coachadd()
    {
        return view('coach-add-client');
    }
    public function coachschedule()
    {
        return view('coach-schedule');
    }
    public function storecookbook(Request $request): RedirectResponse
    {
        $pdf = $request->file('pdf_file');
        $filename = $pdf->getClientOriginalName();
        $pdf->storeAs('pdfs', $filename, 'public');

        $pdfFile = new CookBook();
        $pdfFile->filename = $filename;
        $pdfFile->path = $pdf->store('pdfs', 'public');
        $pdfFile->save();

        return redirect()->back()->with('success', 'PDF uploaded successfully!');

    }
    public function destroy($id)
    {
        $Clientid = User::findOrFail($id);
        $Clientid->delete($id);

        return redirect(RouteServiceProvider::HOME)->with('success',"Client $Clientid->name Deleted Successfully");
    }

    public function carboptions(){
        $carb = CarbOptions::all();
        return view('carb-options' , compact('carb'));
    }
    public function proteinoptions(){
        $protein = ProteinOptions::all();
        return view('protein-options' , compact('protein'));
    }
    public function fatoptions(){
        $fat =FatOptions::all();
        return view('fat-options' , compact('fat'));
    }
    public function vegetableoptions(){
        $vege = VegetableOptions::all();
        return view('vegetable-options' , compact('vege'));
    }
    public function fruitoptions(){
        $fruit = FruitOptions::all();
        return view('fruit-options' , compact('fruit'));
    }
    public function progresspict(){





        // Get the authenticated user
        $user = Auth::user();

        // Get the images submitted by the user, grouped by date
        $imagesByDate = Picture::where('Clientid', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20)
            ->groupBy(function($image) {
                // Format the date to be used as the group key
                return $image->created_at->format('Y-m-d');
            });

        // Get the dates of the images for the pagination links
        $dates = $imagesByDate->keys()->toArray();

        return view('progress-pict', compact('imagesByDate', 'dates'));

    }

}

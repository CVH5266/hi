<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use App\Models\ClientBreakfast;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class DietActionController extends Controller
{
public function show($id)
{
    $client = User::find($id);

    return view('coach/update-client-breakfast',compact('client'));
}

public function update(Request $request , $id)
{

    $request->validate([
        'screenshot' => 'required'
    ]);
    $client = User::find($id);
    $user = Auth::user();
    $oldbreakfast = DB::table('users')
        ->join('clientbreakfast', 'users.id', '=', 'clientbreakfast.Clientid')
        ->select('clientbreakfast.image')->where('clientbreakfast.Coachid', $user->id)->where('clientbreakfast.Clientid', $id);
       $newbreakfast = base64_encode(file_get_contents($request->input('screenshot')));
       $oldbreakfast->update(['image'=>$newbreakfast]);


    return redirect()->route('view-breakfast', ['id' => $id])->with('success', "Breakfast for $client->name updated.");
}
    public function showlunch($id)
    {
        $client = User::find($id);

        return view('coach/update-client-lunch',compact('client'));
    }

    public function updatelunch(Request $request , $id)
    {

        $request->validate([
            'screenshot' => 'required'
        ]);
        $client = User::find($id);
        $user = Auth::user();
        $oldlunch = DB::table('users')
            ->join('clientlunch', 'users.id', '=', 'clientlunch.Clientid')
            ->select('clientlunch.image')->where('clientlunch.Coachid', $user->id)->where('clientlunch.Clientid', $id);
        $newlunch = base64_encode(file_get_contents($request->input('screenshot')));
        $oldlunch->update(['image'=>$newlunch]);


        return redirect()->route('view-lunch', ['id' => $id])->with('success', "Lunch for $client->name updated.");
    }

    public function showdinner($id)
    {
        $client = User::find($id);

        return view('coach/update-client-dinner',compact('client'));
    }

    public function updatedinner(Request $request , $id)
    {

        $request->validate([
            'screenshot' => 'required'
        ]);
        $client = User::find($id);
        $user = Auth::user();
        $olddinner = DB::table('users')
            ->join('clientdinner', 'users.id', '=', 'clientdinner.Clientid')
            ->select('clientdinner.image')->where('clientdinner.Coachid', $user->id)->where('clientdinner.Clientid', $id);
        $newdinner = base64_encode(file_get_contents($request->input('screenshot')));
        $olddinner->update(['image'=>$newdinner]);


        return redirect()->route('view-dinner', ['id' => $id])->with('success', "Dinner for $client->name updated.");
    }


   public function showmeal4($id)
    {
        $client = User::find($id);

        return view('coach/update-client-meal4',compact('client'));
    }

    public function updatemeal4(Request $request , $id)
    {

        $request->validate([
            'screenshot' => 'required'
        ]);
        $client = User::find($id);
        $user = Auth::user();
        $oldmeal4 = DB::table('users')
            ->join('clientmeal4', 'users.id', '=', 'clientmeal4.Clientid')
            ->select('clientmeal4.image')->where('clientmeal4.Coachid', $user->id)->where('clientmeal4.Clientid', $id);
        $newmeal4 = base64_encode(file_get_contents($request->input('screenshot')));
        $oldmeal4->update(['image'=>$newmeal4]);


        return redirect()->route('view-meal4', ['id' => $id])->with('success', "Meal 4 for $client->name updated.");
    }
}

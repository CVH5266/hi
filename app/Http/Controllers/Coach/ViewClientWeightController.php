<?php
namespace App\Http\Controllers\Coach;
use App\Models\CoachClient;
use App\Models\User;
use App\Models\ClientWeight;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class ViewClientWeightController extends Controller
{
    public function show($id){

        $client = User::find($id);
        $weights = ClientWeight::where('Clientid',$client->id)/*->select('weight','created_at')*/->orderBy('added_date')->get();
        $dateweightadded = CoachClient::where('Clientid',$client->id)->get('added_date');
        $initialweight = CoachClient::where('Clientid',$client->id)->get('weight');

        $weight = $initialweight;
        $dataArray1 = json_decode($weight, true);
        $weight = $dataArray1[0]['weight'];

        $data = $dateweightadded;
        $dataArray = json_decode($data, true);
        $date = $dataArray[0]['added_date'];
        return view('coach/client-weight',['weights'=>$weights],compact('client','date','weight'));
    }
}

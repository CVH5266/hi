<?php

namespace App\Http\Controllers\Coach;
use App\Http\Controllers\Controller;
use App\Models\CoachClient;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ViewClientDietController extends Controller
{
public function clientdiet($id)
{
    $user = Auth::user();
    $Clientid = User::find($id);


    $Clienttdee = CoachClient::where('Clientid', $Clientid->id)->get('TDEE');

    $tdee = $Clienttdee;
    $dataArray1 = json_decode($tdee, true);
    $tdee = $dataArray1[0]['TDEE'];

    session()->put('id', $Clientid->id);
    /* $allclients = DB::table('users')
         ->join('coachclient', 'users.id', '=', 'coachclient.Clientid')
         ->select('users.*')->where('coachclient.Coachid', $user->id)
         ->get();*/

    $breakfast = DB::table('users')
        ->join('clientbreakfast', 'users.id', '=', 'clientbreakfast.Clientid')
        ->select('clientbreakfast.image')->where('clientbreakfast.Coachid', $user->id)->where('clientbreakfast.Clientid', $id)
        ->first();

    if ($user && $breakfast) {
        $breakfastimage = $breakfast->image;
        $imagedata = Image::make($breakfastimage);

        return view('coach/view-client-diet', compact('Clientid', 'tdee'))->with(['imagedata' => $imagedata]);
    }
    else{
        $userId = session()->get('id');
        return redirect()->route('diet.create.specific.breakfast',['id' => $userId])->with('warning', "Meal 1 waiting to be created for {$Clientid->name}.");
    }
}
    public function clientlunch($id)
    {
        $user = Auth::user();
        $Clientid = User::find($id);
        $Clienttdee = CoachClient::where('Clientid', $Clientid->id)->get('TDEE');

        $tdee = $Clienttdee;
        $dataArray1 = json_decode($tdee, true);
        $tdee = $dataArray1[0]['TDEE'];
        session()->put('id', $Clientid->id);

        $lunch =DB::table('users')
            ->join('clientlunch', 'users.id', '=', 'clientlunch.Clientid')
            ->select('clientlunch.image')->where('clientlunch.Coachid',$user->id)->where('clientlunch.Clientid',$id)
            ->first();

        if ($user &&  $lunch) {
            $lunchimage = $lunch->image;
            $imagedata = Image::make($lunchimage);

            return view('coach/view-client-lunch', compact('Clientid','tdee'))->with(['imagedata' => $imagedata]);
        }
        else{
            $userId = session()->get('id');
            return redirect()->route('diet.create.specific.lunch', ['id' => $userId])->with('warning', "Meal 2 waiting to be created for {$Clientid->name}.");

        }
    }

    public function clientdinner($id)
    {
        $user = Auth::user();
        $Clientid = User::find($id);
        $Clienttdee = CoachClient::where('Clientid', $Clientid->id)->get('TDEE');

        $tdee = $Clienttdee;
        $dataArray1 = json_decode($tdee, true);
        $tdee = $dataArray1[0]['TDEE'];
        session()->put('id', $Clientid->id);

        $dinner = DB::table('users')
            ->join('clientdinner', 'users.id', '=', 'clientdinner.Clientid')
            ->select('clientdinner.image')->where('clientdinner.Coachid', $user->id)->where('clientdinner.Clientid', $id)
            ->first();
        if ($user &&  $dinner) {
            $dinnerimage = $dinner->image;
            $imagedata = Image::make($dinnerimage);

            return view('coach/view-client-dinner', compact('Clientid','tdee'))->with(['imagedata' => $imagedata]);
        }
        else{
            $userId = session()->get('id');
            return redirect()->route('diet.create.specific.dinner',['id' => $userId])->with('warning', "Meal 3 waiting to be created for {$Clientid->name}.");

        }
    }

    public function clientmeal4($id)
    {
        $user = Auth::user();
        $Clientid = User::find($id);
        $Clienttdee = CoachClient::where('Clientid', $Clientid->id)->get('TDEE');

        $tdee = $Clienttdee;
        $dataArray1 = json_decode($tdee, true);
        $tdee = $dataArray1[0]['TDEE'];
        session()->put('id', $Clientid->id);

        $meal4 = DB::table('users')
            ->join('clientmeal4', 'users.id', '=', 'clientmeal4.Clientid')
            ->select('clientmeal4.image')->where('clientmeal4.Coachid', $user->id)->where('clientmeal4.Clientid', $id)
            ->first();
        if ($user &&  $meal4) {
            $mealimage = $meal4->image;
            $imagedata = Image::make($mealimage);

            return view('coach/view-client-meal4', compact('Clientid','tdee'))->with(['imagedata' => $imagedata]);
        }
        else{
            $userId = session()->get('id');
            return redirect()->route('diet.create.specific.meal4',['id' => $userId])->with('warning', "Meal 4 waiting to be created for {$Clientid->name}.");

        }
    }

}

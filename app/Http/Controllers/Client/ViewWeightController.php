<?php

namespace App\Http\Controllers\Client;
use App\Http\Controllers\Controller;
use App\Models\ClientWeight;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ViewWeightController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'weight' => 'required',
            'date' => 'required',
        ]);
        $user = Auth::user();
        $today = now()->format('Y-m-d');
        $last_weight = ClientWeight::where('Clientid', $user->id)
            ->whereDate('created_at', $today)
            ->orderByDesc('created_at')
            ->first();
        if ($last_weight && $last_weight->created_at->format('Y-m-d') == $today) {
            return redirect()->back()->with('error', "You Only Can Record Once A Day");
        } else {
            $addweight = new ClientWeight();
            $addweight->weight = $request->input('weight');
            $addweight->added_date = $request->input('date');
            $addweight->Clientid = request('Clientid', $user->id);
            $addweight->save();

            return redirect()->back()->with('success', "Weight For Today Recorded.");
        }
    }
}

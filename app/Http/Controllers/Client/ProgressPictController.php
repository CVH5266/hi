<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Picture;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class ProgressPictController extends Controller
{
    public function store(Request $request)
    {
        $user_id = auth()->user()->id;

// Get the current date
        $currentDate = Carbon::now()->toDateString();


// Get the number of images submitted by the user on the current date
        $numImages = Picture::where('Clientid', $user_id)->whereDate('created_at', $currentDate)->count();

// Check if the user has already submitted 6 images today
        if ($numImages >= 6) {
            return redirect()->back()->with('error', 'You have reached the maximum number of image submissions for today.');
        } else {



            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $image) {
                    $path = $image->store('public/images');
                    Picture::create([
                        'Clientid' => $user_id,
                        'path' => str_replace('public/', '', $path),
                    ]);
                }
            }

            /*$pictures = $user->pictures; //get all pictures belonging to logged in user*/

            return redirect()->back()->with('success', "Image Uploaded.");
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ClientDinner;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AddDinnerController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'screenshot' => ['required'],
        ]);

        $usercoach = Auth::user();
        $selectedclient= $request->input('clientid');
        $exists = DB::table('ClientDinner')
            ->where('Clientid', $selectedclient)
            ->exists();

        //get name and decode using json to string//
        $user = User::where('id',$selectedclient)->get('name');
        $data =  $user;
        $dataArray = json_decode($data, true);
        $name = $dataArray[0]['name'];
        //end
        if ($exists) {
            return redirect()->back()->with('error', "$name already has a dinner plan");
        } else {
            $screenshot = base64_encode(file_get_contents($request->input('screenshot')));
            $addBreakfast = new ClientDinner();
            $addBreakfast->image = $screenshot;
            $addBreakfast->Coachid = $request->input('Coachid', $usercoach->id);
            $addBreakfast->Clientid = $request->input('Clientid', $selectedclient);
            $addBreakfast->save();


            return redirect()->route('view-dinner', ['id' =>  $selectedclient])->with('success', "Dinner for $name Created Successfully");
        }
    }
}

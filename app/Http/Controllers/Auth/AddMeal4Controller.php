<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ClientMeal4;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AddMeal4Controller extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'screenshot' => ['required'],
        ]);

        $usercoach = Auth::user();
        $selectedclient= $request->input('clientid');
        $exists = DB::table('ClientMeal4')
            ->where('Clientid', $selectedclient)
            ->exists();

        //get name and decode using json to string//
        $user = User::where('id',$selectedclient)->get('name');
        $data =  $user;
        $dataArray = json_decode($data, true);
        $name = $dataArray[0]['name'];
        //end
        if ($exists) {
            return redirect()->back()->with('error', "$name already has a Meal 4");
        } else {
            $screenshot = base64_encode(file_get_contents($request->input('screenshot')));
            $addBreakfast = new ClientMeal4();
            $addBreakfast->image = $screenshot;
            $addBreakfast->Coachid = $request->input('Coachid', $usercoach->id);
            $addBreakfast->Clientid = $request->input('Clientid', $selectedclient);
            $addBreakfast->save();


            return redirect()->route('view-meal4', ['id' =>  $selectedclient])->with('success', "Meal 4 for $name Created Successfully");
        }
    }
}

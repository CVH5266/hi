<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Models\FoodNutritionFacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateClientMeal4Controller extends Controller
{
    public function dietmeal4()
    {
        $user = Auth::user();

        $allclients = DB::table('users')
            ->join('coachclient', 'users.id', '=', 'coachclient.Clientid')
            ->select('users.*')->where('coachclient.Coachid', $user->id)
            ->get();

        $foodnutritions = FoodNutritionFacts::all();
        return view('diet-create-meal4', compact('foodnutritions','allclients'));
    }

    public function dietsearch(Request $request)
    {
        $output = " ";
        $nutritionfacts = FoodNutritionFacts::where('Food', 'Like', '%' . $request->
            search . '%')->get();

        foreach ($nutritionfacts as $nutritionfact) {
            $output .=
                '<tr>
                <td>' . $nutritionfact->Food . '</td>
                <td>' . $nutritionfact->Carbohydrates . '</td>
                  <td>' . $nutritionfact->Protein . '</td>
                 <td>' . $nutritionfact->Fat . '</td>
                  <td>' . $nutritionfact->Calories . '</td>

                  <td>'
                . '<button  data-item-id="' . $nutritionfact->id . '
                "
                 id="get-item-details">
                ' . 'Add</button>' .
                '</td>

                 </tr>';

        }
        return response($output);
    }

    public function dietadd(Request $request)
    {
        $itemId = $request->input('id');
        $item = FoodNutritionFacts::find($itemId);
        return response()->json(['item' => $item]);
    }
}

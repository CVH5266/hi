<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Models\CoachClient;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Auth;

class CoachAddClientController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'dob' =>$request->dob,
            'date' =>$request->weight,
            'TDEE' =>$request->TDEE,
        ]);

        $user->attachRole('client');
        event(new Registered($user));

        $usercoach = Auth::user();
        $addClientid = new CoachClient();
        $addClientid ->Clientid=$request->input('Clientid',$user->id);
        $addClientid->Clientname= request('name');
        $addClientid->Clientemail= request('email');
        $addClientid->added_date=request('date');
        $addClientid->weight=request('weight');
        $addClientid->TDEE=request('TDEE');
        $addClientid->Coachid=$request->input('Coachid',$usercoach->id);
        $addClientid->Coachemail=$request->input('Coachemail',$usercoach->email);
        $addClientid->Coachname=$request->input('Coachname',$usercoach->name);
        $addClientid->save();


        return redirect(RouteServiceProvider::HOME)->with('success',"Client $user->name Created Successfully");
    }
}

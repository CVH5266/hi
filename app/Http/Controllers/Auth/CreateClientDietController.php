<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Models\CoachClient;
use App\Models\FoodNutritionFacts;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class CreateClientDietController extends Controller
{
    public function dietshow()
    {
        $user = Auth::user();

        $allclients = DB::table('users')
            ->join('coachclient', 'users.id', '=', 'coachclient.Clientid')
            ->select('users.*')->where('coachclient.Coachid', $user->id)
            ->get();

        $foodnutritions = FoodNutritionFacts::all();
        return view('diet-create', compact('foodnutritions','allclients'));
    }

    public function dietsearch(Request $request)
    {
        $output = " ";
        $nutritionfacts = FoodNutritionFacts::where('Food', 'Like', '%' . $request->
            search . '%')->get();

        foreach ($nutritionfacts as $nutritionfact) {
            $output .=
                '<tr>
                <td>' . $nutritionfact->Food . '</td>
                <td>' . $nutritionfact->Carbohydrates . '</td>
                  <td>' . $nutritionfact->Protein . '</td>
                 <td>' . $nutritionfact->Fat . '</td>
                  <td>' . $nutritionfact->Calories . '</td>
                    <td>' . $nutritionfact->Unit . '</td>
                  <td>'
                . '<button  data-item-id="' . $nutritionfact->id . '
                "
                 id="get-item-details">
                ' . 'Add</button>' .
                '</td>

                 </tr>';

        }
        return response($output);
    }

    public function dietadd(Request $request)
    {
        $itemId = $request->input('id');
        $item = FoodNutritionFacts::find($itemId);
        return response()->json(['item' => $item]);
    }
    public function specificbreakfast($id)
    {
        $user = User::find($id);
        $tdee = CoachClient::where('Clientid',$user->id)->get('TDEE');
        $foodnutritions = FoodNutritionFacts::all();

        $tdee1 =  $tdee;
        $dataArray1 = json_decode($tdee1, true);
        $tdee1 = $dataArray1[0]['TDEE'];
        return view('coach/diet-create-specific-breakfast', compact('foodnutritions','user','tdee1'));
    }
    public function specificlunch($id)
    {
        $user = User::find($id);
        $tdee = CoachClient::where('Clientid',$user->id)->get('TDEE');
        $foodnutritions = FoodNutritionFacts::all();
        $tdee1 =  $tdee;
        $dataArray1 = json_decode($tdee1, true);
        $tdee1 = $dataArray1[0]['TDEE'];
        return view('coach/diet-create-specific-lunch', compact('foodnutritions','user','tdee1'));
    }
    public function specificdinner($id)
    {
        $user = User::find($id);
        $tdee = CoachClient::where('Clientid',$user->id)->get('TDEE');
        $foodnutritions = FoodNutritionFacts::all();
        $tdee1 =  $tdee;
        $dataArray1 = json_decode($tdee1, true);
        $tdee1 = $dataArray1[0]['TDEE'];
        return view('coach/diet-create-specific-dinner', compact('foodnutritions','user','tdee1'));
    }
    public function specificmeal4($id)
    {
        $user = User::find($id);
        $tdee = CoachClient::where('Clientid',$user->id)->get('TDEE');
        $foodnutritions = FoodNutritionFacts::all();
        $tdee1 =  $tdee;
        $dataArray1 = json_decode($tdee1, true);
        $tdee1 = $dataArray1[0]['TDEE'];
        return view('coach/diet-create-specific-meal4', compact('foodnutritions','user','tdee1'));
    }



}


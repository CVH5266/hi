<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CoachClient extends Model
{
    protected $table = 'coachclient';
 /*   public function coach()
    {
        return $this->belongsTo(User::class);
    }*/
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'Clientname',
        'Clientemail',
        'Clientid',
        'Coachid',
        'Coachname',
        'Coachemail',
        'date',
        'weight',
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];
}


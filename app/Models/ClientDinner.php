<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ClientDinner extends Model
{
    protected $table = 'clientdinner';

    protected $fillable = [
        'screenshot',
        'Clientid',
        'Coachid',
    ];
}

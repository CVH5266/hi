<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodNutritionFacts extends Model
{
    protected $table = 'food_nutrition_facts';
    protected $fillable = ['Food', 'Calories'];

}

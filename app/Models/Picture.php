<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Picture extends  Model
{
    protected $table = 'progress_pictures';

    protected $fillable = ['path', 'Clientid'];

}

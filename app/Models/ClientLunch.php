<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ClientLunch extends Model
{
    protected $table = 'clientlunch';

    protected $fillable = [
        'screenshot',
        'Clientid',
        'Coachid',
    ];

}

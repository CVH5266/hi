<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ClientMeal4 extends Model
{
    protected $table = 'clientmeal4';

    protected $fillable = [
        'screenshot',
        'Clientid',
        'Coachid',
    ];

}

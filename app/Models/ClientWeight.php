<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientWeight extends Model
{
    protected $table = 'clientweight';

    protected $fillable = [
        'Clientid',
        'weight',
        'date',
    ];
}

<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class CookBook extends Model
{
    protected $table = 'cookbook';

    protected $fillable = ['filename', 'path'];
}

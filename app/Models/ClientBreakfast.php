<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ClientBreakfast extends Model
{
    protected $table = 'clientbreakfast';

    protected $fillable = [
        'screenshot',
        'Clientid',
        'Coachid',
    ];

}

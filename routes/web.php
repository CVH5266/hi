<?php

use App\Http\Controllers\Auth\CoachAddClientController;
use App\Http\Controllers\ProfileController;
use App\Models\FoodNutritionFacts;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\CreateClientDietController;
/*use GuzzleHttp\Client;*/


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//auth route for both
Route::group(['middleware' => ['auth']], function() {
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
});
/*Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');*/

// for clients
Route::group(['middleware' => ['auth', 'role:client']], function() {
    Route::get('/dashboard/diet', 'App\Http\Controllers\DashboardController@clientdiet')->name('dashboard.client-diet');
    Route::get('/dashboard/workout', 'App\Http\Controllers\DashboardController@clientworkout')->name('dashboard.client-workout');
    Route::get('/dashboard/eat-out-options', 'App\Http\Controllers\DashboardController@eatout')->name('eat-out-options');
    Route::get('download-cookbook/', 'App\Http\Controllers\DashboardController@downloadcookbook')->name('download-cookbook');
    Route::post('/upload-cookbook', 'App\Http\Controllers\DashboardController@storecookbook')->name('submit-cookbook');

    Route::get('/dashboard/carbs-options', 'App\Http\Controllers\DashboardController@carboptions')->name('carbs-options');
    Route::get('/dashboard/protein-options', 'App\Http\Controllers\DashboardController@proteinoptions')->name('protein-options');
    Route::get('/dashboard/fat-options', 'App\Http\Controllers\DashboardController@fatoptions')->name('fat-options');
    Route::get('/dashboard/fruits-options', 'App\Http\Controllers\DashboardController@fruitoptions')->name('fruits-options');
    Route::get('/dashboard/vegetable-options', 'App\Http\Controllers\DashboardController@vegetableoptions')->name('vegetable-options');


    Route::post('/dashboard/weight', 'App\Http\Controllers\Client\ViewWeightController@store')->name('submit-own-weight');

    Route::get('/dashboard/progress-pict', 'App\Http\Controllers\DashboardController@progresspict')->name('progress-pict');
    Route::post('/dashboard/submit-progress-pict', 'App\Http\Controllers\Client\ProgressPictController@store')->name('submit-picture');




});

// for coaches
Route::group(['middleware' => ['auth', 'role:coach']], function () {
    Route::get('/dashboard/coach-create', 'App\Http\Controllers\DashboardController@coachcreate')->name('dashboard.coach-create');
    Route::get('/dashboard/add-client', 'App\Http\Controllers\DashboardController@coachadd')->name('dashboard.coach-add-client');
    Route::get('/dashboard/schedule', 'App\Http\Controllers\DashboardController@coachschedule')->name('dashboard.coach-schedule');
    Route::get('register-client', [CoachAddClientController::class, 'create'])
        ->name('register-client');
    Route::post('register-client', [CoachAddClientController::class, 'store']);
    Route::delete('/delete-client/{id}','App\Http\Controllers\DashboardController@destroy')->name('client.destroy');
    Route::get('/create/client-diet/breakfast', 'App\Http\Controllers\Auth\CreateClientDietController@dietshow')->name('diet.create');

    //error handling
    Route::get('/create/client-diet/{id}/breakfast', 'App\Http\Controllers\Auth\CreateClientDietController@specificbreakfast')->name('diet.create.specific.breakfast');
    Route::get('/create/client-diet/{id}/lunch', 'App\Http\Controllers\Auth\CreateClientDietController@specificlunch')->name('diet.create.specific.lunch');
    Route::get('/create/client-diet/{id}/dinner', 'App\Http\Controllers\Auth\CreateClientDietController@specificdinner')->name('diet.create.specific.dinner');
    //
    Route::get('/create/client-diet/{id}/meal4', 'App\Http\Controllers\Auth\CreateClientDietController@specificmeal4')->name('diet.create.specific.meal4');

    Route::get('/create/client-diet/lunch', 'App\Http\Controllers\Auth\CreateClientLunchController@dietlunch')->name('diet.create.lunch');
    Route::get('/create/client-diet/dinner', 'App\Http\Controllers\Auth\CreateClientDinnerController@dietdinner')->name('diet.create.dinner');
    //
    Route::get('/create/client-diet/meal4', 'App\Http\Controllers\Auth\CreateClientMeal4Controller@dietmeal4')->name('diet.create.meal4');

    Route::get('/search', 'App\Http\Controllers\Auth\CreateClientDietController@dietsearch');
    Route::post('/get-food-data/', 'App\Http\Controllers\Auth\CreateClientDietController@dietadd')->name('get-food-data');
    Route::post('/submit-breakfast/','App\Http\Controllers\Auth\AddBreakfastController@store' )->name('submit-breakfast');
    Route::post('/submit-lunch/','App\Http\Controllers\Auth\AddLunchController@store' )->name('submit-lunch');
    Route::post('/submit-dinner/','App\Http\Controllers\Auth\AddDinnerController@store' )->name('submit-dinner');
    //
    Route::post('/submit-meal4/','App\Http\Controllers\Auth\AddMeal4Controller@store' )->name('submit-meal4');

    //breakfast
    Route::get('/view/{id}/breakfast', 'App\Http\Controllers\Coach\ViewClientDietController@clientdiet')->name('view-breakfast');
    Route::get('/update/{id}/breakfast', 'App\Http\Controllers\Coach\DietActionController@show')->name('update-breakfast');
    Route::put('/breakfast/{id}/update', 'App\Http\Controllers\Coach\DietActionController@update')->name('update-data');
    //lunch
    Route::get('/view/{id}/lunch', 'App\Http\Controllers\Coach\ViewClientDietController@clientlunch')->name('view-lunch');
    Route::get('/update/{id}/lunch', 'App\Http\Controllers\Coach\DietActionController@showlunch')->name('update-lunch');
    Route::put('/lunch/{id}/update', 'App\Http\Controllers\Coach\DietActionController@updatelunch')->name('update-lunch-data');
    //dinner
    Route::get('/view/{id}/dinner', 'App\Http\Controllers\Coach\ViewClientDietController@clientdinner')->name('view-dinner');
    Route::get('/update/{id}/dinner', 'App\Http\Controllers\Coach\DietActionController@showdinner')->name('update-dinner');
    Route::put('/dinner/{id}/update', 'App\Http\Controllers\Coach\DietActionController@updatedinner')->name('update-dinner-data');
    //meal4
    Route::get('/view/{id}/meal4', 'App\Http\Controllers\Coach\ViewClientDietController@clientmeal4')->name('view-meal4');
    Route::get('/update/{id}/meal4', 'App\Http\Controllers\Coach\DietActionController@showmeal4')->name('update-meal4');
    Route::put('/meal4/{id}/update', 'App\Http\Controllers\Coach\DietActionController@updatemeal4')->name('update-meal4-data');

    Route::get('/view/{id}/weight', 'App\Http\Controllers\Coach\ViewClientWeightController@show')->name('view-client-weight');

});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

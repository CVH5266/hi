<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carb_options', function (Blueprint $table) {
            $table->id();
            $table->string('Food');
            $table->string('Units');
            $table->string('Carbohydrate');
            $table->string('Protein');
            $table->string('Fat');
            $table->string('Calories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};

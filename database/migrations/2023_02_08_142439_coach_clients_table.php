<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CoachClient', function (Blueprint $table) {
            $table->id();
            $table->string('Clientname');
            $table->string('Clientemail');
            $table->foreignId('Clientid')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->string('Coachname');
            $table->string('Coachemail');
            $table->foreignId('Coachid')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CoachClient');
    }
};

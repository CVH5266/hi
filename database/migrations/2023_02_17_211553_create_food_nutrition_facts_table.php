<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_nutrition_facts', function (Blueprint $table) {
            $table->id();
            $table->string('Food');
            $table->string('Calories');
            $table->string('Protein');
            $table->string('Carbohydrates');
            $table->string('Fat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_nutrition_facts');
    }
};

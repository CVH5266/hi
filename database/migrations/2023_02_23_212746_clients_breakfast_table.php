<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ClientBreakfast', function (Blueprint $table) {
            $table->id();
            $table->binary('image');
            $table->foreignId('Clientid')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('Coachid')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ClientBreakfast');
    }
};

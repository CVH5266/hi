<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __("Welcome Back,") }} {{Auth::user()->name}}.
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg ">
                <div class="p-6 text-gray-900">
                    {{ __('Your Clients') }}
                    <br>
                    @include('flash-message')
                   <br>
                    <div style="overflow-x:auto;">
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>D.O.B</th>
                            <th>Action</th>
                        </tr>
                        @foreach($allclients as $allclient)
                            <tr>
                                <td class="client-info">
                                    {{$allclient->name}}
                                </td>
                                <td class="client-info">
                                    {{$allclient->email}}
                                </td>
                                <td class="client-info">
                                    {{$allclient->dob}}
                                </td>
                                <td class="d-flex action justify-content-around">

                                    <button class="btn-action"><a href="{{ route('view-breakfast',  ['id' => $allclient->id])}}" class="{{ request()->routeIs('view-breakfast') ? 'active' : '' }}" ><img src="{{ asset('images/eyes.png') }}" alt="Your image" height="auto" width="25px"></a></button>


                                    <button class="btn-action" type="button" data-toggle="modal" data-target="#deleteUserModal{{ $allclient->id }} ">
                                        <img  src="{{ asset('images/trash.png') }}" alt="Your image" height="auto" width="25px">
                                    </button>

                                    <div class="modal fade" id="deleteUserModal{{ $allclient->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteUserModal{{ $allclient->id }}Label" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteUserModal{{$allclient->id }}Label">Confirm User Deletion</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure you want to delete <strong>"{{$allclient->name }}"</strong>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <form action="{{ route('client.destroy', $allclient->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--<form class="trash-btn " action="{{ route('client.destroy',  $allclient->id)}}"
                                          method="POST" >
                                        @csrf
                                        @method('DELETE')

                                        <button value="{{$allclient->id}}" id="delete-user" >
                                           <img src="{{ asset('images/trash.png') }}" alt="Your image" height="auto" width="25px">
                                        </button>

                                    </form>--}}

                                    <button class="btn-action"><a href="{{ route('view-client-weight',  ['id' => $allclient->id])}}" class="{{ request()->routeIs('view-client-weight') ? 'active' : '' }}" ><img  class="trash-btn " src="{{ asset('images/weight-scale.png') }}" alt="Your image" height="auto" width="25px"></a></button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<scrip>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</scrip>


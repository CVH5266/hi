@if ($message = Session::get('success'))
    <div x-data="{ showMessage: true }" x-show="showMessage" x-init="setTimeout(() => showMessage = false, 5000)" >
         <div class="alert alert-success" role="alert">
        <strong>{{ $message }}</strong>
        </div>
    </div>
@endif


@if ($message = Session::get('error'))
    <div x-data="{ showMessage: true }" x-show="showMessage" x-init="setTimeout(() => showMessage = false, 5000)" >
        <div class="alert alert-danger" role="alert">
        <strong>{{ $message }}</strong>
        </div>
    </div>
@endif


@if ($message = Session::get('warning'))
    <div class="alert alert-warning mt-5" role="alert" >
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($message = Session::get('info'))
    <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif


@if ($errors->any())
    <div class="alert alert-danger ">
        <button type="button" class="close" data-dismiss="alert">×</button>
        Make Sure Every Field Is Filled
    </div>
@endif

<x-app-layout>
    <x-slot name="header">
        <div class=" ms-2">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight ">
                {{ __('Creating Meal 1: ') }}
            </h2>
        </div>
        <div class="row mt-3" style="text-align: center">
            <div class="col-12 col-md-12 col-lg-3 mb-sm-2 mb-2">
                <a href="{{ route('diet.create') }} " class=" meall {{ request()->routeIs('diet.create') ? 'active' : '' }}">
                    <button class="btn font-semibold text-xl  leading-tight ">Meal 1</button>
                </a>
            </div>
            <div class="col-12 col-md-12 col-lg-3 mb-sm-2 mb-2">
                <a href="{{ route('diet.create.lunch') }} ">
                    <button class="btn  font-semibold text-xl  leading-tight ">Meal 2</button>
                </a>
            </div>
            <div class="col-12 col-md-12 col-lg-3 mb-sm-2 mb-2">
                <a href="{{ route('diet.create.dinner') }} ">
                    <button class="btn  font-semibold text-xl  leading-tight ">Meal 3</button>
                </a>
            </div>
            <div class="col-12 col-md-12 col-lg-3">
                <a href="{{ route('diet.create.meal4') }} ">
                    <button class="btn  font-semibold text-xl  leading-tight ">Meal 4</button>
                </a>
            </div>
        </div>
    </x-slot>
    <div style="width: 50%; text-align: center;margin: 0 auto" class="mt-5"> @include('flash-message') </div>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="  p-6 text-gray-900   ">

                    <div class="search">
                        <input type="search"
                               name="search"
                               id="search"
                               placeholder="Search For Food &#x1F50D;"
                               class="form-control mb-5">
                    </div>
                    <div class="table-container mb-5 " >
                        <table id="foods" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Carbohydrates(g)</th>
                                <th>Protein(g)</th>
                                <th>Fat(g)</th>
                                <th>Calories(kcal)</th>
                                <th>Unit(g)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody class="allfood">
                            </tbody>

                            <tbody id="Content" class="searchfood">
                            </tbody>

                        </table>

                    </div>


{{--//change here//--}}
                    <div class="table-container ">
                        <div class="d-flex justify-between">
                            <p class="mb-2 table-title">Meal 1</p>
                            {{--   <button onclick="addRow()" CLASS="btn btn-info"> Add Food</button>--}}
                        </div>
                        <form method="POST" action="{{ route('submit-breakfast') }}" enctype="multipart/form-data">
                            @csrf
                            <table id="food">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Carbohydrates(g)</th>
                                    <th>Protein(g)</th>
                                    <th>Fat(g)</th>
                                    <th>Calories(kcal)</th>
                                    <th>Unit(g)</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>


                                <tbody id="item-details" class="searchfood">
                                </tbody>

                                <tbody>

                                    <th>Total</th>
                                    <td id="carbs" style="color: red"></td>
                                    <td id="protein" style="color: red"></td>
                                    <td id="fat" style="color: red"></td>
                                    <td id="calories" style="color: red"></td>
                                    <td></td>
                                    <td></td>

                                </tbody>

                            </table>
                            <label for="client" class="mt-2">Choose a client:</label>
                            <select name="clientid" class="form-control" >Select Client
                                @foreach($allclients as $allclient)
                                    <option value="{{$allclient->id}}">{{$allclient->name}}</option>
                                @endforeach
                            </select>
                            <div class="mt-2 d-flex">
                                <input type="hidden" id="screenshot-input" name="screenshot">
                                <button id="btn-submit" class="btn btn-success " type="submit">SEND</button>
                            </div>
                        </form>
                    </div>
                    <strong class="output-text mt-2">Output:</strong>
                    <div class="mt-2" id="output" ></div>
                    <div class="d-flex justify-content-between">
                        <button class="mt-2 btn-action" id="remove-capture"><img  src="{{ asset('images/trash.png') }}" alt="Your image" height="auto" width="25px"></button>
                        <button class="btn-info" id="capture-screenshot">SAVE</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>



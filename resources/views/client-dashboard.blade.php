<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __("Welcome Back,") }} <strong>{{Auth::user()->name}}</strong>.{{ __('Your weight progress is shown by the graph below') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div style="width: 50%; text-align: center;margin: 0 auto"> @include('flash-message') </div>
                    <canvas id="weight-chart" class="mb-5"></canvas>
                    <script>
                        const weights = @json($weights);
                        const chartCanvas = document.getElementById('weight-chart').getContext('2d');
                        const chart = new Chart(chartCanvas, {
                            type: 'line',
                            data: {
                                labels: weights.map(w => w.added_date),
                                datasets: [{
                                    label: 'Weight(KG)',
                                    data: weights.map(w => w.weight),
                                    backgroundColor:'rgba(255,99,132,0.2)',
                                    borderColor: 'rgba(255,99,132,1)',
                                    fill: false
                                }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: 'Weight History'
                                },
                                scales: {
                                    xAxes: [{
                                        type: 'time',
                                        time: {
                                            unit: 'day'
                                        }
                                    }],
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    </script>
                    <form method="POST" action="{{ route('submit-own-weight') }}" enctype="multipart/form-data" class="mt-2" >
                        @csrf
                        <input type="number" name="weight" step=".01" class="form-control mb-2" placeholder="Enter Your Weight.">
                       {{-- <input type="date" name="date" class="form-control mb-2" >--}}
                       <input type="date" name="date" class="form-control mb-2" value="{{ date('Y-m-d') }}" readonly>
                        <button class="btn btn-success "> submit</button>

                    </form>
                </div>
            </div>
        </div>
    </div>


{{--   <form method="POST" action="{{ route('submit-cookbook') }}" enctype="multipart/form-data">
    @csrf
        <input type="file" name="pdf_file">
        <button type="submit"> submit</button>
    </form>--}}
</x-app-layout>

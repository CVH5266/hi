<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
        {{--<Style>--}}

        <link rel="stylesheet" href="{{ URL::asset('/css/main.css') }}" >

        {{--bootsrap--}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
        <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>




    </head>
    <style>
        footer{
            text-align: center;
            font-weight: bold;
        }
       a:hover{
            text-decoration: none;
        }
    </style>
    <body class="font-sans antialiased">

    <button id="scroll-to-top" title="Go to top">&#8613;</button>
    <button id="scroll-to-bottom" title="Go to bottom">&#8615;</button>

    <div class="min-h-screen bg-gray-100">
        @include('layouts.navigation')
        <!-- Page Heading -->
        @if (isset($header))
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>
        @endif

            <!-- Page Content -->
            <main>
                {{ $slot }}
                <footer>
                    <p class="foot">
                    Copyright 2023 ChinTech
                    </p>
                </footer>
            </main>
        </div>
        <script type="text/javascript">
            $('#search').on('keyup',function ()

            {
                $value=$(this).val();
                if($value)
                {
                    $('.allfood').hide();
                    $('.searchfood').show();

                }
                else
                {
                    $('.allfood').show();
                    $('.searchfood').hide();
                }
                $.ajax({

                    type:'get',
                    url:'{{URL::to('search')}}',
                    data:{'search':$value},

                    success:function (data)
                    {
                        console.log(data);
                        $('#Content').html(data);
                    }

                });
            });
///
            const hideButton3 = document.querySelector('#capture-screenshot');
            hideButton3.style.display = 'none';

            $(document).on('click', '#get-item-details', function(event) {
                event.preventDefault();
                var itemId = $(this).data('item-id');
                var formData = {
                    'id': itemId,
                    '_token': '{{ csrf_token() }}'
                };
                $.ajax({
                    url: '{{ route("get-food-data") }}',
                    method: 'POST',
                    data: formData,
                    success: function(response) {
                        var item = response.item;
                        const showButton3 = document.querySelector('#capture-screenshot');
                        showButton3.style.display = 'block';


                        var newRow = $('<tr >'+
                            '<input id="food1" type="hidden" name="food-name[]">'+
                            '<td class ="td1">'+ item.Food +'</td>' +
                            '<input id="food2" type="hidden" name="carbs[]" >' +
                            '<td class ="td2">'+ item.Carbohydrates + '</td>' +
                            '<input id="food3" type="hidden" name="protein[]" >' +
                            '<td class ="td3"> '+ item.Protein + '</td>' +
                            '<input id="food4" type="hidden" name="fat[]" >'+
                            '<td class ="td4"> '+item.Fat + '</td>' +
                            '<input id="food5" type="hidden" name="calories[]" >' +
                            '<td class ="td5"> '+ item.Calories + '</td>'+
                            '<td class ="td6">'   + item.Unit + '</td>'+
                            '<td> <button class="remove"> X </button> </td>'+
                            ' </tr>'
                    );

                        $('#item-details').append(newRow);

                        /*$('.td1').each(function() {
                            var cellText = $(this).text();
                            $(this).siblings('input[id="food1"]').val(cellText);
                        });
                        $('.td2').each(function() {
                            var cellText = $(this).text();
                            $(this).siblings('input[id="food2"]').val(cellText);
                        });
                        $('.td3').each(function() {
                            var cellText = $(this).text();
                            $(this).siblings('input[id="food3"]').val(cellText);
                        });
                        $('.td4').each(function() {
                            var cellText = $(this).text();
                            $(this).siblings('input[id="food4"]').val(cellText);
                        });
                        $('.td5').each(function() {
                            var cellText = $(this).text();
                            $(this).siblings('input[id="food5"]').val(cellText);
                        });*/
                        $(document).ready(function() {
                            var sum = 0; // initialize the sum to 0

                            // loop through the td elements in the specified column
                            $('.td2').each(function() {
                                // extract the numeric value from the td element and add it to the sum
                                sum += parseFloat($(this).text());
                            });

                            // display the sum in the specified total td element
                            $('#carbs').text(sum.toFixed(2));
                        });

                        $(document).ready(function() {
                            var sum = 0; // initialize the sum to 0

                            // loop through the td elements in the specified column
                            $('.td3').each(function() {
                                // extract the numeric value from the td element and add it to the sum
                                sum += parseFloat($(this).text());
                            });

                            // display the sum in the specified total td element
                            $('#protein').text(sum.toFixed(2));
                        });
                        $(document).ready(function() {
                            var sum = 0; // initialize the sum to 0

                            // loop through the td elements in the specified column
                            $('.td4').each(function() {
                                // extract the numeric value from the td element and add it to the sum
                                sum += parseFloat($(this).text());
                            });

                            // display the sum in the specified total td element
                            $('#fat').text(sum.toFixed(2));
                        });
                        $(document).ready(function() {
                            var sum = 0; // initialize the sum to 0

                            // loop through the td elements in the specified column
                            $('.td5').each(function() {
                                // extract the numeric value from the td element and add it to the sum
                                sum += parseFloat($(this).text());
                            });

                            // display the sum in the specified total td element
                            $('#calories').text(sum.toFixed(2));
                        });
                    }
                });
            });

           /* var total = parseFloat($('#carbs').text());
            var selectedValue =parseFloat($(this).closest('td.td2').text());
            var result = total - selectedValue;
            $('#carbs').text(result.toFixed(2));*/

            $('#item-details').on('click','.remove', function() {
                $(document).ready(function() {
                    var minus = 0; // initialize the sum to 0

                    // loop through the td elements in the specified column
                    $('.td2').each(function() {
                        // extract the numeric value from the td element and add it to the sum
                        minus -= parseFloat($(this).text());
                    });

                    // display the sum in the specified total td element
                    $('#carbs').text(Math.abs(minus.toFixed(2)));
                });

                $(document).ready(function() {
                    var minus = 0; // initialize the sum to 0

                    // loop through the td elements in the specified column
                    $('.td3').each(function() {
                        // extract the numeric value from the td element and add it to the sum
                        minus -= parseFloat($(this).text());
                    });

                    // display the sum in the specified total td element
                    $('#protein').text(Math.abs(minus.toFixed(2)));
                });
                $(document).ready(function() {
                    var minus = 0; // initialize the sum to 0

                    // loop through the td elements in the specified column
                    $('.td4').each(function() {
                        // extract the numeric value from the td element and add it to the sum
                        minus -= parseFloat($(this).text());
                    });

                    // display the sum in the specified total td element
                    $('#fat').text(Math.abs(minus.toFixed(2)));
                });
                $(document).ready(function() {
                    var minus = 0; // initialize the sum to 0

                    // loop through the td elements in the specified column
                    $('.td5').each(function() {
                        // extract the numeric value from the td element and add it to the sum
                        minus -= parseFloat($(this).text());
                    });

                    // display the sum in the specified total td element
                    $('#calories').text(Math.abs(minus.toFixed(2)));
                });
                $(this).closest('tr').remove();
            });
            ///////
            const hideButton = document.querySelector('#remove-capture');
            hideButton.style.display = 'none';
            const hideSUBMITButton = document.querySelector('#btn-submit');
            hideSUBMITButton.style.display = 'none';
            const hidetext = document.querySelector('.output-text');
            hidetext.style.display = 'none';

            //////////
            $('#capture-screenshot').click(function() {
                const  showButton = document.querySelector('#remove-capture');
                showButton.style.display = 'block';
                const showSUBMITButton = document.querySelector('#btn-submit');
                showSUBMITButton.style.display = 'block';
                const hideButton3 = document.querySelector('#capture-screenshot');
                hideButton3.style.display = 'none';
                const hidetext = document.querySelector('.output-text');
                hidetext.style.display = 'block';

                const screenshotTarget = document.getElementById('food');
                html2canvas(screenshotTarget).then(canvas => {
                    // to image as png use below line
                    // const base64image = canvas.toDataURL("image/png");
                    // show the image in window use below line
                    // window.location.href = base64image;

                    // screenshot appended to the body as canvas
                    document.getElementById('output').appendChild(canvas);
                    canvas.style.width = "100%";
                    canvas.style.height = "auto";
                    dataURL = canvas.toDataURL();

                    // Use the html2canvas
                    // function to take a screenshot
                    // and append it
                    // to the output div
                    $('#remove-capture').click(function() {
                        canvas.remove();
                        const hideButton = document.querySelector('#remove-capture');
                        hideButton .style.display = 'none';
                        const hideButton3 = document.querySelector('#capture-screenshot');
                        hideButton3.style.display = 'block';
                        const hideButton4 = document.querySelector('#btn-submit');
                        hideButton4.style.display = 'none';
                        const hidetext = document.querySelector('.output-text');
                        hidetext.style.display = 'none';
                        $("#screenshot-input").val("");


                    });
                    const screenshotInput = document.getElementById('screenshot-input');
                        screenshotInput.value = dataURL;
                });
            });


            var myButton = document.getElementById("scroll-to-top");
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {
                scrollFunction()
            };
            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    myButton.classList.add("show");
                } else {
                    myButton.classList.remove("show");
                }
            }
            // When the user clicks on the button, scroll to the top of the document
            myButton.onclick = function() {
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            };


        </script>
    </body>
</html>



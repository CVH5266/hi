<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Something For Clients Today, ') }}
            <strong>{{Auth::user()->name}}</strong>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class=" row p-6 text-gray-900   justify-content-around ">
                    <a class="col-4 btn btn-primary" href="{{ route('diet.create') }}"><button>{{ __("Diet Plan") }}</button></a>
                    <a  class="col-4 btn btn-secondary"><button >{{ __("Workout Plan") }}</button></a>
                </div>
                {{--<div class="container">Client with complete meal plan</div>--}}
            </div>
        </div>
    </div>
</x-app-layout>

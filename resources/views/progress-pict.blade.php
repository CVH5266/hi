<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My Progress Picture.') }}
        </h2>
    </x-slot>

    <div style="width: 50%; text-align: center;margin: 0 auto" class="mt-5"> @include('flash-message') </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <form action="{{ route('submit-picture') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row" style="width: 100%;margin: 0 auto">
                        <input type="file" name="images[]" multiple class="form-control col-12 col-md-12 col-lg-6" id="images">
                        <button type="submit" class="form-control col-12 col-md-12 col-lg-6">Upload Pictures</button>
                        </div>
                    </form>

                    <div class="mt-3">
                        <div class="container">
                            @if ($imagesByDate->isEmpty())
                                <p>No images found.</p>
                            @else
                                @foreach ($imagesByDate as $date => $images)
                                    <h2>{{ $date }}</h2>
                                    <div class="row">
                                        @foreach ($images as $image)
                                            <div class="col-md-3 mb-3">
                                                <div class="thumbnail">
                                                    <a href="{{ asset($image->path) }}" target="_blank">
                                                        <img src="{{ asset('storage/' . $image->path) }}" alt="{{ $image->title }}">
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            @endif

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    // Get the input element
    const input = document.getElementById('images');

    // Add an event listener for when files are selected
    input.addEventListener('change', () => {
        // Get the selected files
        const files = input.files;

        // Check if more than 6 files were selected
        if (files.length > 6) {
            // Remove the excess files
            for (let i = 6; i < files.length; i++) {
                input.value = '';
            }
            // Show an error message
            alert('You can only select up to 6 images.');
        }
    });
</script>

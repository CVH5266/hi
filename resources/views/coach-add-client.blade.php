<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Fill Up Client Information.') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                        <form method="POST" action="{{ route('register-client') }}">
                            @csrf

                            <!-- Name -->
                            <div>
                                <x-input-label for="name" :value="__('Name')" />
                                <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                                <x-input-error :messages="$errors->get('name')" class="mt-2" />
                            </div>

                            <!-- Email Address -->
                            <div class="mt-4">
                                <x-input-label for="email" :value="__('Email')" />
                                <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                                <x-input-error :messages="$errors->get('email')" class="mt-2" />
                            </div>
                            <!-- Age -->
                            <div class="mt-4">
                                <x-input-label for="dob" :value="__('Date Of Birth')" />
                                <x-text-input id="dob" class="block mt-1 w-full" type="date" name="dob" :value="old('dob')" required />
                                <x-input-error :messages="$errors->get('dob')" class="mt-2" />
                            </div>
                            <!-- Client Weight -->
                            <div class="mt-4 row ">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                <x-input-label for="weight" :value="__('Client Weight')" />
                                <x-text-input id="weight" class="block mt-1 w-full" type="number" name="weight" :value="old('weight')" step=".01" placeholder="Enter Your Client Weight." required />
                                <x-input-error :messages="$errors->get('weight')" class="mt-2" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                    <x-input-label for="TDEE" :value="__('Client TDEE')" />
                                    <x-text-input id="TDEE" class="block mt-1 w-full" type="number" name="TDEE" :value="old('TDEE')"  step=".01" placeholder="Enter Your Client TDEE." required />
                                    <x-input-error :messages="$errors->get('TDEE')" class="mt-2" />
                                </div>
                            </div>
                            <!-- Password -->
                            <div class="mt-4">
                                <x-input-label for="password" :value="__('Password')" />

                                <x-text-input id="password" class="block mt-1 w-full"
                                              type="password"
                                              name="password"
                                              required autocomplete="new-password" />

                                <x-input-error :messages="$errors->get('password')" class="mt-2" />
                            </div>

                            <!-- Confirm Password -->
                            <div class="mt-4">
                                <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                                <x-text-input id="password_confirmation" class="block mt-1 w-full"
                                              type="password"
                                              name="password_confirmation" required />

                                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
                            </div>

                            <input type="date" name="date" class="form-control mb-2" style="display: none" value="{{ date('Y-m-d') }}" readonly>
                           {{-- <!-- Select Option Rol type -->
                            <div class="mt-4">
                                <x-input-label for="role_id" value="{{ __('Register as:') }}"/>
                                <select name="role_id"
                                        class="block mt-1 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                                    <option value="client">Client</option>
                                </select>
                            </div>--}}

                            <div class="flex items-center justify-end mt-4">
                                <x-primary-button class="ml-4">
                                    {{ __('Register') }}
                                </x-primary-button>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>

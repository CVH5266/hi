<x-app-layout>
    <x-slot name="header">

            <h2 class="font-semibold text-xl text-gray-800 leading-tight  mb-3">
                Customizable Plan / Food Reference:
            </h2>

        <button class="collapsible mt-2 ">Click to Read</button>
        <div class="content">
            <p class="pt-3 pb-3"><strong>The following are some food references for you if you would like to change your daily food of choice.
                    Please ensure all food modification should still be within your daily calories & macros intake as stated
                    above, either by using hand written calculation or MyFitnessPal app. Do contact your coach if you are having any
                    trouble with your food modifications.</strong>
            </p>
        </div>

        <strong><p class="long-text mt-3">The <a href="{{ route('download-cookbook')}}" style="color: red">Cook Book</a> that provide by Coach Julian is a low calorie home cook meals for your
                reference if you are bored with the same food everyday. As mentioned above, please ensure all food
                modification should still be within your daily calories & macros intake as stated above. Do contact your coach
                if you are having any trouble with your food modifications.

            </p></strong>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 pt-lg-3" style="text-align: center">
                    <div class="row" id="link-box">
                        <a  class="col-lg-2 col-12" href="{{ route('eat-out-options') }} ">Eat Out Options</a>
                        <a class="col-lg-2 col-12" href="{{ route('carbs-options') }} "> Carbs</a>
                        <a class="col-lg-2 col-12 options {{ request()->routeIs('protein-options') ? 'active' : '' }}" href="{{ route('protein-options') }} "> Protein</a>
                        <a class="col-lg-2 col-12" href="{{ route('fat-options') }} "> Fat</a>
                        <a class="col-lg-2 col-12" href="{{ route('fruits-options') }} "> Fruits</a>
                        <a class="col-lg-2 col-12" href="{{ route('vegetable-options') }} "> Vegetable</a>
                    </div>
                </div>
                <div class="p-6 text-gray-900">
                   {{-- <h2 class="font-semibold text-xl text-gray-800 leading-tight pb-4">
                        {{ __('Protein Options:') }}
                    </h2>--}}
                    <div style="overflow-x: auto">
                    <table>
                        <tr>
                            <th>N0</th>
                            <th>Food</th>
                            <th>Units</th>
                            <th>Carbohydrates</th>
                            <th>Protein</th>
                            <th>Fat</th>
                            <th>Calories</th>
                        </tr>

                        @foreach($protein as $number=>$item)
                            <tr>
                                <td>{{$number+1 }}</td>
                                <td>{{$item->Food}}</td>
                                <td>{{$item->Units}}</td>
                                <td>{{$item->Carbohydrate}}</td>
                                <td>{{$item->Protein}}</td>
                                <td>{{$item->Fat}}</td>
                                <td>{{$item->Calories}}</td>
                            </tr>
                        @endforeach




                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
</script>

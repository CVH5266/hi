<x-app-layout>
    <x-slot name="header">
        <div style="text-align: center">
            <p>Updating Meal 4 for  <strong>{{$client->name}}</strong></p>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="  p-6 text-gray-900   ">

                    <div class="search">
                        <input type="search"
                               name="search"
                               id="search"
                               placeholder="Search For Food &#x1F50D;"
                               class="form-control mb-5">
                    </div>
                    <div class="table-container mb-5 " >
                        <table id="foods" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Carbohydrates(g)</th>
                                <th>Protein(g)</th>
                                <th>Fat(g)</th>
                                <th>Calories(kcal)</th>
                                <th>Unit(g)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody class="allfood">
                            </tbody>

                            <tbody id="Content" class="searchfood">
                            </tbody>

                        </table>

                    </div>


                    {{--//change here//--}}
                    <div class="table-container ">
                        <div class="d-flex justify-between">
                            <p class="mb-2 table-title">Meal 4</p>
                            {{--   <button onclick="addRow()" CLASS="btn btn-info"> Add Food</button>--}}
                        </div>
                        <form  method="POST" action="{{ route('update-meal4-data', $client->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <table id="food">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Carbohydrates(g)</th>
                                    <th>Protein(g)</th>
                                    <th>Fat(g)</th>
                                    <th>Calories(kcal)</th>
                                    <th>Unit(g / PerServing)</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>


                                <tbody id="item-details" class="searchfood">
                                </tbody>

                                <tbody>

                                <th>Total</th>
                                <td id="carbs" style="color: red"></td>
                                <td id="protein" style="color: red"></td>
                                <td id="fat" style="color: red"></td>
                                <td id="calories" style="color: red"></td>
                                <td></td>
                                <td></td>

                                </tbody>

                            </table>
                            <input type="hidden" id="screenshot-input" name="screenshot">
                            <button id="btn-submit" class="btn btn-success mt-2 " type="submit">UPDATE</button>
                        </form>
                    </div>
                    <strong class="output-text">Result:</strong>
                    <div class="mt-2" id="output" ></div>
                    <div class="d-flex justify-content-between">
                        <button class="mt-2 btn-action" id="remove-capture"><img  src="{{ asset('images/trash.png') }}" alt="Your image" height="auto" width="25px"></button>
                        <button class="btn-info " id="capture-screenshot">save</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>

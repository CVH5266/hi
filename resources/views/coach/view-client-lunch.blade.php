<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Meal 2 for ,') }} {{$Clientid->name}}
            <br><br>
            TDEE: <strong style="color: darkred">{{$tdee}}Kcal</strong>
        </h2>
        <div style="width: 50%; text-align: center;margin: 0 auto" class="mt-5 mb-2"> @include('flash-message') </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="  p-5 text-gray-900    ">
                    <div class="d-flex">
                        <a href="{{ route('view-breakfast',  ['id' => $Clientid->id])}}" class="meal1 {{ request()->routeIs('view-breakfast') ? 'active' : '' }}">Meal 1</a>
                        <a href="{{ route('view-lunch',  ['id' => $Clientid->id])}}" class="meal {{ request()->routeIs('view-lunch') ? 'active' : '' }}">Meal 2</a>
                        <a href="{{ route('view-dinner',  ['id' => $Clientid->id])}}" class="meal {{ request()->routeIs('view-dinner') ? 'active' : '' }}">Meal 3</a>
                        <a href="{{ route('view-meal4',  ['id' => $Clientid->id])}}"class="meal {{ request()->routeIs('view-meal4') ? 'active' : '' }}">Meal 4</a>
                    </div>
                    <img class="client-breakfast mt-1" src="{{ $imagedata->encode('data-url') }}" alt="File">
                    <div class="mt-2 pb-2"><button class="update-btn"><a href="{{ route('update-lunch',  ['id' => $Clientid->id])}}">Update</a></button></div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

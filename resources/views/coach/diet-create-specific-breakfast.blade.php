<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <div style="width: 50%; text-align: center;margin: 0 auto"> @include('flash-message') </div>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="  p-6 text-gray-900   ">

                    <div class="search">
                        <input type="search"
                               name="search"
                               id="search"
                               placeholder="Search Bar"
                               class="form-control mb-5">
                    </div>
                    <div class="table-container mb-5 " >
                        <table id="foods" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Carbohydrates(g)</th>
                                <th>Protein(g)</th>
                                <th>Fat(g)</th>
                                <th>Calories(kcal)</th>
                                <th>Unit(g)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody class="allfood">
                            </tbody>

                            <tbody id="Content" class="searchfood">
                            </tbody>

                        </table>

                    </div>


                    {{--//change here//--}}
                    <div class="table-container ">
                        <div class="d-flex justify-between">
                            <p class="mb-2 table-title">Creating Meal 1 for <Strong class="selected-name">"{{$user->name}}"</Strong> - TDEE: "<Strong class="selected-name">{{$tdee1}}</Strong> KCAL"</p>
                            {{--   <button onclick="addRow()" CLASS="btn btn-info"> Add Food</button>--}}
                        </div>
                        <form method="POST" action="{{ route('submit-breakfast') }}" enctype="multipart/form-data">
                            @csrf
                            <table id="food">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Carbohydrates(g)</th>
                                    <th>Protein(g)</th>
                                    <th>Fat(g)</th>
                                    <th>Calories(kcal)</th>
                                    <th>Unit(g / PerServing)</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>


                                <tbody id="item-details" class="searchfood">
                                </tbody>

                                <tbody>

                                <th>Total</th>
                                <td id="carbs" style="color: red"></td>
                                <td id="protein" style="color: red"></td>
                                <td id="fat" style="color: red"></td>
                                <td id="calories" style="color: red"></td>
                                <td></td>
                                <td></td>

                                </tbody>

                            </table>

                            <div class="mt-2 d-flex ">
                                <select name="clientid" class="mt-2 select-tag" style="display: none">Select Client
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                </select>
                                <input type="hidden" id="screenshot-input" name="screenshot">
                                <button id="btn-submit" class="btn btn-success " type="submit">SEND</button>
                            </div>
                        </form>
                    </div>
                    <strong class="output-text">Result:</strong>
                    <div class="mt-2" id="output" ></div>
                    <div class="d-flex justify-content-between">
                        <button class="mt-2 btn-action" id="remove-capture"><img  src="{{ asset('images/trash.png') }}" alt="Your image" height="auto" width="25px"></button>
                        <button class="btn-info " id="capture-screenshot">save</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>

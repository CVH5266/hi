<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <p><strong class="client-name mb-2">{{$client->name}}</strong>'s weight</p>
            <p><strong class="client-name">{{$client->name}}</strong>'s initial weight as of <strong style="color: darkred">{{$date}}</strong>: <strong style="color: darkred">{{$weight}}KG</strong> </p>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                  {{--  <p>
                    @foreach($weight as $row)
                        {{$row->weight}}
                    @endforeach
                    </p>--}}

                    <canvas id="weight-chart"></canvas>
                    <script>
                        const weights = @json($weights);
                        const chartCanvas = document.getElementById('weight-chart').getContext('2d');
                        const chart = new Chart(chartCanvas, {
                            type: 'line',
                            data: {
                                labels: weights.map(w => w.added_date),
                                datasets: [{
                                    label: 'Weight(KG)',
                                    data: weights.map(w => w.weight),
                                    backgroundColor:'rgba(255,99,132,0.2)',
                                    borderColor: 'rgba(255,99,132,1)',
                                    fill: false
                                }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: 'Weight History'
                                },
                                scales: {
                                    xAxes: [{
                                        type: 'time',
                                        time: {
                                            unit: 'day'
                                        }
                                    }],
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: false
                                        }
                                    }]
                                }
                            }
                        });
                    </script>



                </div>
            </div>
        </div>
    </div>


    {{--  <form method="POST" action="{{ route('submit-cookbook') }}" enctype="multipart/form-data">
      @csrf
          <input type="file" name="pdf_file">
          <button type="submit"> submit</button>
      </form>--}}
</x-app-layout>
